# Common Http Entity Wrapper

This library support :

- Standard API response
- Standard API paging and sorting request
- Error handler wrapper
- Simplify JSON Generator
- Generate OpenAPI Spec
- Swagger UI

## Setup

To use this library, setup our pom.xml

```xml
<dependency>
    <groupId>com.gitlab.residwi</groupId>
    <artifactId>spring-library-common</artifactId>
    <version>0.0.1-SNAPSHOT</version>
</dependency>

<!-- add repository -->
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/groups/12070658/-/packages/maven</url>
    </repository>
</repositories>
```

## Standard Web Response

Always use class `ResponsePayload<T>`.

```java
/**
 * Standard Web Response
 */
public class ResponsePayload<T> {

    /**
     * Code , usually same as HTTP Code
     */
    private Integer code;

    /**
     * Status, usually same as HTTP status
     */
    private String status;

    /**
     * Response data
     */
    private T data;

    /**
     * Paging information, if response is paginate data
     */
    private Paging paging;

    /**
     * Error information, if request is not valid 
     */
    private Map<String, List<String>> errors;

    /**
     * Metadata information
     */
    private Map<String, Object> metadata;
}
```

We can use `ResponseHelper` to construct `ResponsePayload<T>` object.

```java

@RestController
public class CustomerController {

    @GetMapping(value = "/api/customers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponsePayload<Customer> findById(@PathVariable("id") String id) {
        Customer customer = service.findById(id);
        return ResponseHelper.ok(customer);
    }
}
```

## Standard Paging Request and Response

For Paging, we can use `PagingRequest` class for web request, and `Paging` class for web response. We also already
implement `PagingRequestArgumentResolver` to support argument injection on the controller, so we don't need to parse
paging request manually.

```java

@RestController
public class CustomerController {

    @GetMapping(
            value = "/api/customers",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponsePayload<List<Customer>> list(PagingRequest pagingRequest) {
        return customerService.find(pagingRequest)
                .map(response -> {
                    Paging paging = PagingHelper.toPaging(pagingRequest, response.getTotal());
                    return ResponseHelper.ok(response.getCustomers(), paging);
                });
    }
}
``` 

In the url we can use standard paging :

```http request
GET /api/customers?page=1&item_per_page=100

GET /api/customers?page=2&item_per_page=50

GET /api/customers?page=2
```

We can also configure default paging properties :

```properties
library.common.paging.default-page=1
library.common.paging.default-item-per-page=50
library.common.paging.max-item-per-page=1000
```

## Standard Sorting Request

In the `PagingRequest` we also can give sorting information, and it will automatically injected to `PagingRequest`.

```http request
GET /api/customers?page=2&sort_by=id:asc,first_name:asc,created_at:desc

GET /api/customers?page=2&sort_by=id,first_name,created_at:desc
```

We can also configure default sorting properties

```properties
library.common.paging.default-sort-direction=asc
```

## Default Error Handler

Common library also contain default error handler to create standard error response. We can use `CommonErrorException`
class to create standard error controller.

```java

@RestControllerAdvice
public class ErrorController implements CommonErrorException, MessageSourceAware {

    private static final Logger log = LoggerFactory.getLogger(ErrorController.class);

    private MessageSource messageSource;

    @Override
    public Logger getLogger() {
        return log;
    }

    @Override
    public MessageSource getMessageSource() {
        return messageSource;
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
``` 

## Open API and Swagger UI

This library will automatically generate open api spec and swagger ui. This library will read all Controller class and
automatically generate open api spec.

```http request
# Open API Spec
GET /v3/api-docs

# Swagger UI
GET /swagger-ui.html
```

We can also make Paging & Sorting feature to be included on Swagger OpenAPI Spec. We only need to add
annotation `@PagingRequestInQuery`

```java

@RestController
public class CustomerController {

    @PagingRequestInQuery
    @GetMapping(
            value = "/api/customers",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponsePayload<List<Customer>> list(PagingRequest pagingRequest) {
        // some logic
    }
}
```

To configure swagger information on Open API Spec.

```properties
library.common.swagger.title=@project.artifactId@
library.common.swagger.version=@project.version@
library.common.swagger.description=@project.description@
library.common.swagger.terms-of-service=https://www.example.com/
```

## Metadata Information for Validation

Sometimes we want to send metadata information for UI in validation message. We can do this with add `@MetaDatas` on
validation field

```java
public class SampleRequest {

    @NotBlank(message = "NotBlank")
    private String name;

    @MetaDatas({
            @MetaData(key = "min", value = "1"),
            @MetaData(key = "max", value = "100")
    })
    @Min(1)
    @Max(100)
    private Integer age;
}
```

When get validation error, response will automatically contains metadata

```json
{
  "code": 400,
  "status": "BAD_REQUEST",
  "errors": {
    "age": [
      "TooLarge"
    ]
  },
  "metadata": {
    "errors": {
      "age": {
        "min": "1",
        "max": "100"
      }
    }
  }
}
```

## Simplify JSON Generator

### Automatic Jackson Configuration

This library by default will configure some configuration. So we don't need to configure it manually on application
properties.

```properties
spring.jackson.default-property-inclusion=non_null
spring.jackson.generator.ignore-unknown=true
spring.jackson.mapper.accept-case-insensitive-enums=true
spring.jackson.serialization.fail-on-empty-beans=false
spring.jackson.serialization.write-empty-json-arrays=true
spring.jackson.serialization.write-dates-as-timestamps=true
spring.jackson.serialization.write-date-keys-as-timestamps=true
spring.jackson.serialization.write-char-arrays-as-json-arrays=true
spring.jackson.deserialization.read-unknown-enum-values-as-null=true
spring.jackson.deserialization.fail-on-ignored-properties=false
spring.jackson.deserialization.fail-on-unknown-properties=false
```

### Generate JSON using JsonHelper Class

We don't need to use `ObjectMapper` anymore to manipulate json data. We can use `JsonHelper` class, so we don't need to
add try-catch manually anymore.

```java
public class Sample {

    @Autowired
    private JsonHelper jsonHelper;

    // Parse Json
    Model model = jsonHelper.fromJson(stringJson, Model.class);
    Map<String, Object> map = jsonHelper.fromJson(stringJson, new TypeReference<Map<String, Object>>() {
    });

    // Generate Json
    String json = jsonHelper.toJson(model);
}
```