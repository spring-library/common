package com.gitlab.residwi.spring.library.common;

import com.gitlab.residwi.spring.library.common.properties.PagingProperties;
import com.gitlab.residwi.spring.library.common.properties.SwaggerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableConfigurationProperties({
        PagingProperties.class,
        SwaggerProperties.class
})
@PropertySource(
        ignoreResourceNotFound = true,
        value = "classpath:common.properties"
)
public class CommonAutoConfiguration {
}
