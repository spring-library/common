package com.gitlab.residwi.spring.library.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.common.factory.json.JsonAwareBeanPostProcessor;
import com.gitlab.residwi.spring.library.common.helper.JsonHelper;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;

@AutoConfigureAfter({
        CommonAutoConfiguration.class,
        JacksonAutoConfiguration.class
})
public class CommonJsonAutoConfiguration {

    @Bean
    public JsonHelper jsonHelper(ObjectMapper objectMapper) {
        return new JsonHelper(objectMapper);
    }

    @Bean
    public JsonAwareBeanPostProcessor jsonAwareBeanPostProcessor(JsonHelper jsonHelper) {
        return new JsonAwareBeanPostProcessor(jsonHelper);
    }

}
