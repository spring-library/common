package com.gitlab.residwi.spring.library.common;

import com.gitlab.residwi.spring.library.common.properties.PagingProperties;
import com.gitlab.residwi.spring.library.common.resolver.PagingRequestArgumentResolver;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@ConditionalOnClass(WebMvcConfigurer.class)
@AutoConfigureAfter(CommonAutoConfiguration.class)
public class CommonPagingRequestAutoConfiguration implements WebMvcConfigurer {

    private final PagingProperties pagingProperties;

    public CommonPagingRequestAutoConfiguration(PagingProperties pagingProperties) {
        this.pagingProperties = pagingProperties;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new PagingRequestArgumentResolver(pagingProperties));
    }
}
