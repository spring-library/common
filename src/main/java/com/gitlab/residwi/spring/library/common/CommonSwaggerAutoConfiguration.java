package com.gitlab.residwi.spring.library.common;

import com.gitlab.residwi.spring.library.common.factory.swagger.ComponentsFactoryBean;
import com.gitlab.residwi.spring.library.common.factory.swagger.OpenAPIFactoryBean;
import com.gitlab.residwi.spring.library.common.properties.PagingProperties;
import com.gitlab.residwi.spring.library.common.properties.SwaggerProperties;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.parameters.Parameter;
import io.swagger.v3.oas.models.parameters.QueryParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter(CommonAutoConfiguration.class)
public class CommonSwaggerAutoConfiguration {

    @Bean
    public ComponentsFactoryBean components(ApplicationContext applicationContext) {
        var parameters = applicationContext.getBeansOfType(Parameter.class);

        var componentsFactoryBean = new ComponentsFactoryBean();
        componentsFactoryBean.setParameters(parameters);
        return componentsFactoryBean;
    }

    @Bean
    public OpenAPIFactoryBean openAPI(@Autowired Components components,
                                      @Autowired SwaggerProperties swaggerProperties) {
        var openAPIFactoryBean = new OpenAPIFactoryBean();
        openAPIFactoryBean.setComponents(components);
        openAPIFactoryBean.setSwaggerProperties(swaggerProperties);
        return openAPIFactoryBean;
    }

    @Bean
    public Parameter queryPagingRequestPage(PagingProperties pagingProperties) {
        return new QueryParameter()
                .name(pagingProperties.getQuery().getPageKey())
                .example(pagingProperties.getDefaultPage())
                .required(true);
    }

    @Bean
    public Parameter queryPagingRequestItemPerPage(PagingProperties pagingProperties) {
        return new QueryParameter()
                .name(pagingProperties.getQuery().getItemPerPageKey())
                .example(pagingProperties.getDefaultItemPerPage())
                .required(true);
    }

    @Bean
    public Parameter queryPagingRequestSortBy(PagingProperties pagingProperties) {
        return new QueryParameter()
                .name(pagingProperties.getQuery().getSortByKey())
                .required(false);
    }

}
