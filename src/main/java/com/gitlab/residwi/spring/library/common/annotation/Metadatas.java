package com.gitlab.residwi.spring.library.common.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Metadatas {

    Metadata[] value();
}
