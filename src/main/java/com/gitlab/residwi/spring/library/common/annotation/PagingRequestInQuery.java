package com.gitlab.residwi.spring.library.common.annotation;

import io.swagger.v3.oas.annotations.Parameter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Parameter(name = "page", ref = "queryPagingRequestPage")
@Parameter(name = "item_per_page", ref = "queryPagingRequestItemPerPage")
@Parameter(name = "sort_by", ref = "queryPagingRequestSortBy")
@Target({
        ElementType.TYPE,
        ElementType.METHOD
})
@Retention(RetentionPolicy.RUNTIME)
public @interface PagingRequestInQuery {
}
