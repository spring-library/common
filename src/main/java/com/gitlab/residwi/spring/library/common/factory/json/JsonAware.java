package com.gitlab.residwi.spring.library.common.factory.json;

import com.gitlab.residwi.spring.library.common.helper.JsonHelper;
import org.springframework.beans.factory.Aware;

public interface JsonAware extends Aware {

    void setJsonHelper(JsonHelper jsonHelper);
}
