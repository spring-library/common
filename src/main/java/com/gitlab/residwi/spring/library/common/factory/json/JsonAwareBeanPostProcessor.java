package com.gitlab.residwi.spring.library.common.factory.json;

import com.gitlab.residwi.spring.library.common.helper.JsonHelper;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class JsonAwareBeanPostProcessor implements BeanPostProcessor {

    private final JsonHelper jsonHelper;

    public JsonAwareBeanPostProcessor(JsonHelper jsonHelper) {
        this.jsonHelper = jsonHelper;
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof JsonAware) {
            var jsonAware = (JsonAware) bean;
            jsonAware.setJsonHelper(jsonHelper);
        }

        return bean;
    }
}
