package com.gitlab.residwi.spring.library.common.factory.swagger;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.parameters.Parameter;
import org.springframework.beans.factory.FactoryBean;

import java.util.Map;

public class ComponentsFactoryBean implements FactoryBean<Components> {

    private Map<String, Parameter> parameters;

    @Override
    public Components getObject() {
        var components = new Components();
        components.setParameters(parameters);
        return components;
    }

    @Override
    public Class<?> getObjectType() {
        return Components.class;
    }

    public void setParameters(Map<String, Parameter> parameters) {
        this.parameters = parameters;
    }
}
