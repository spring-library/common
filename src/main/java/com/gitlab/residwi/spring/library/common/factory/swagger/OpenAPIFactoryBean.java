package com.gitlab.residwi.spring.library.common.factory.swagger;

import com.gitlab.residwi.spring.library.common.properties.SwaggerProperties;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.FactoryBean;

public class OpenAPIFactoryBean implements FactoryBean<OpenAPI> {

    private SwaggerProperties swaggerProperties;

    private Components components;

    @Override
    public OpenAPI getObject() {
        var openAPI = new OpenAPI();
        openAPI.setInfo(getInfo());
        openAPI.setComponents(components);
        return openAPI;
    }

    private Info getInfo() {
        var info = new Info();
        info.setTitle(swaggerProperties.getTitle());
        info.setDescription(swaggerProperties.getDescription());
        info.setTermsOfService(swaggerProperties.getTermsOfService());
        info.setVersion(swaggerProperties.getVersion());
        return info;
    }

    @Override
    public Class<?> getObjectType() {
        return OpenAPI.class;
    }

    public void setSwaggerProperties(SwaggerProperties swaggerProperties) {
        this.swaggerProperties = swaggerProperties;
    }

    public void setComponents(Components components) {
        this.components = components;
    }
}