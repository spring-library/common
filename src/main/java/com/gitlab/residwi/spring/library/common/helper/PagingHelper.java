package com.gitlab.residwi.spring.library.common.helper;

import com.gitlab.residwi.spring.library.common.model.request.PagingRequest;
import com.gitlab.residwi.spring.library.common.model.response.PagingPayload;

public class PagingHelper {

    private PagingHelper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static PagingPayload toPaging(PagingRequest request, Long totalItem) {
        var totalPage = totalItem / request.getItemPerPage();
        if (totalItem % request.getItemPerPage() != 0) {
            totalPage++;
        }
        return toPaging(request, totalPage, totalItem);
    }

    public static PagingPayload toPaging(PagingRequest request, Long totalPage, Long totalItem) {
        return new PagingPayload(request.getPage(), totalPage, request.getItemPerPage(), totalItem, request.getSortBy());
    }

}
