package com.gitlab.residwi.spring.library.common.helper;

import com.gitlab.residwi.spring.library.common.model.response.PagingPayload;
import com.gitlab.residwi.spring.library.common.model.response.ResponsePayload;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;

public class ResponseHelper {

    private ResponseHelper() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

    public static <T> ResponsePayload<T> ok() {
        return ResponseHelper.ok(null);
    }

    public static <T> ResponsePayload<T> ok(T data) {
        return ResponseHelper.ok(data, null);
    }

    public static <T> ResponsePayload<T> ok(T data, PagingPayload pagingPayload) {
        return ResponseHelper.status(HttpStatus.OK, data, pagingPayload);
    }

    public static <T> ResponsePayload<T> internalServerError() {
        return ResponseHelper.status(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static <T> ResponsePayload<T> unauthorized() {
        return ResponseHelper.status(HttpStatus.UNAUTHORIZED);
    }

    public static <T> ResponsePayload<T> badRequest(Map<String, List<String>> errors) {
        return ResponseHelper.status(HttpStatus.BAD_REQUEST, null, null, errors);
    }

    public static <T> ResponsePayload<T> status(HttpStatus status) {
        return ResponseHelper.status(status, null);
    }

    public static <T> ResponsePayload<T> status(HttpStatus status, T data) {
        return ResponseHelper.status(status, data, null);
    }

    public static <T> ResponsePayload<T> status(HttpStatus status, T data, PagingPayload pagingPayload) {
        return ResponseHelper.status(status, data, pagingPayload, null);
    }

    public static <T> ResponsePayload<T> status(HttpStatus status, T data, PagingPayload pagingPayload, Map<String, List<String>> errors) {
        return new ResponsePayload<>(status.value(), status.name(), data, pagingPayload, errors, null);
    }
}
