package com.gitlab.residwi.spring.library.common.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.Hidden;

import java.util.List;

@Hidden
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PagingRequest {

    private Long page;

    private Long itemPerPage;

    private List<SortByRequest> sortBy;

    public PagingRequest() {
    }

    public PagingRequest(Long page, Long itemPerPage, List<SortByRequest> sortBy) {
        this.page = page;
        this.itemPerPage = itemPerPage;
        this.sortBy = sortBy;
    }

    public Long getPage() {
        return this.page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getItemPerPage() {
        return this.itemPerPage;
    }

    public void setItemPerPage(Long itemPerPage) {
        this.itemPerPage = itemPerPage;
    }

    public List<SortByRequest> getSortBy() {
        return this.sortBy;
    }

    public void setSortBy(List<SortByRequest> sortBy) {
        this.sortBy = sortBy;
    }
}
