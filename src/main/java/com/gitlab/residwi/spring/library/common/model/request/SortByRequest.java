package com.gitlab.residwi.spring.library.common.model.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SortByRequest {

    private String propertyName;

    private SortType type;

    public SortByRequest() {
    }

    public SortByRequest(String propertyName, SortType type) {
        this.propertyName = propertyName;
        this.type = type;
    }

    public String getPropertyName() {
        return this.propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public SortType getType() {
        return this.type;
    }

    public void setType(SortType type) {
        this.type = type;
    }
}
