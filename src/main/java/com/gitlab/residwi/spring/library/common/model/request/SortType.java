package com.gitlab.residwi.spring.library.common.model.request;

public enum SortType {
    ASC, DESC
}
