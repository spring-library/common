package com.gitlab.residwi.spring.library.common.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.gitlab.residwi.spring.library.common.model.request.SortByRequest;

import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PagingPayload {

    private Long page;

    private Long totalPage;

    private Long itemPerPage;

    private Long totalItem;

    private List<SortByRequest> sortBy;

    public PagingPayload(Long page, Long totalPage, Long itemPerPage, Long totalItem, List<SortByRequest> sortBy) {
        this.page = page;
        this.totalPage = totalPage;
        this.itemPerPage = itemPerPage;
        this.totalItem = totalItem;
        this.sortBy = sortBy;
    }

    public PagingPayload() {
    }

    public Long getPage() {
        return this.page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getTotalPage() {
        return this.totalPage;
    }

    public void setTotalPage(Long totalPage) {
        this.totalPage = totalPage;
    }

    public Long getItemPerPage() {
        return this.itemPerPage;
    }

    public void setItemPerPage(Long itemPerPage) {
        this.itemPerPage = itemPerPage;
    }

    public Long getTotalItem() {
        return this.totalItem;
    }

    public void setTotalItem(Long totalItem) {
        this.totalItem = totalItem;
    }

    public List<SortByRequest> getSortBy() {
        return this.sortBy;
    }

    public void setSortBy(List<SortByRequest> sortBy) {
        this.sortBy = sortBy;
    }
}
