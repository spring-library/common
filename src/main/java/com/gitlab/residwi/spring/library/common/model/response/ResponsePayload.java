package com.gitlab.residwi.spring.library.common.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.List;
import java.util.Map;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ResponsePayload<T> {

    private Integer code;

    private String status;

    private T data;

    private PagingPayload paging;

    private Map<String, List<String>> errors;

    private Map<String, Object> metadata;

    public ResponsePayload() {
    }

    public ResponsePayload(Integer code, String status, T data, PagingPayload paging, Map<String, List<String>> errors, Map<String, Object> metadata) {
        this.code = code;
        this.status = status;
        this.data = data;
        this.paging = paging;
        this.errors = errors;
        this.metadata = metadata;
    }

    public Integer getCode() {
        return this.code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public PagingPayload getPaging() {
        return this.paging;
    }

    public void setPaging(PagingPayload paging) {
        this.paging = paging;
    }

    public Map<String, List<String>> getErrors() {
        return this.errors;
    }

    public void setErrors(Map<String, List<String>> errors) {
        this.errors = errors;
    }

    public Map<String, Object> getMetadata() {
        return this.metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }
}