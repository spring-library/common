package com.gitlab.residwi.spring.library.common.properties;

import com.gitlab.residwi.spring.library.common.model.request.SortType;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("library.common.paging")
public class PagingProperties {

    private Long defaultPage = 1L;

    private Long defaultItemPerPage = 10L;

    private SortType defaultSortType = SortType.ASC;

    private Long maxItemPerPage = 100L;

    private Query query = new Query();

    public PagingProperties(Long defaultPage, Long defaultItemPerPage, SortType defaultSortType, Long maxItemPerPage, Query query) {
        this.defaultPage = defaultPage;
        this.defaultItemPerPage = defaultItemPerPage;
        this.defaultSortType = defaultSortType;
        this.maxItemPerPage = maxItemPerPage;
        this.query = query;
    }

    public PagingProperties() {
    }

    public Long getDefaultPage() {
        return this.defaultPage;
    }

    public void setDefaultPage(Long defaultPage) {
        this.defaultPage = defaultPage;
    }

    public Long getDefaultItemPerPage() {
        return this.defaultItemPerPage;
    }

    public void setDefaultItemPerPage(Long defaultItemPerPage) {
        this.defaultItemPerPage = defaultItemPerPage;
    }

    public SortType getDefaultSortType() {
        return this.defaultSortType;
    }

    public void setDefaultSortType(SortType defaultSortType) {
        this.defaultSortType = defaultSortType;
    }

    public Long getMaxItemPerPage() {
        return this.maxItemPerPage;
    }

    public void setMaxItemPerPage(Long maxItemPerPage) {
        this.maxItemPerPage = maxItemPerPage;
    }

    public Query getQuery() {
        return this.query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    public static class Query {

        private String pageKey = "page";

        private String itemPerPageKey = "item_per_page";

        private String sortByKey = "sort_by";

        public Query(String pageKey, String itemPerPageKey, String sortByKey) {
            this.pageKey = pageKey;
            this.itemPerPageKey = itemPerPageKey;
            this.sortByKey = sortByKey;
        }

        public Query() {
        }

        public String getPageKey() {
            return this.pageKey;
        }

        public void setPageKey(String pageKey) {
            this.pageKey = pageKey;
        }

        public String getItemPerPageKey() {
            return this.itemPerPageKey;
        }

        public void setItemPerPageKey(String itemPerPageKey) {
            this.itemPerPageKey = itemPerPageKey;
        }

        public String getSortByKey() {
            return this.sortByKey;
        }

        public void setSortByKey(String sortByKey) {
            this.sortByKey = sortByKey;
        }
    }
}
