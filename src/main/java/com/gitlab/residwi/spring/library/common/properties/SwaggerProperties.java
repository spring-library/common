package com.gitlab.residwi.spring.library.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("library.common.swagger")
public class SwaggerProperties {

    private String title;

    private String description;

    private String termsOfService;

    private String version;

    public SwaggerProperties(String title, String description, String termsOfService, String version) {
        this.title = title;
        this.description = description;
        this.termsOfService = termsOfService;
        this.version = version;
    }

    public SwaggerProperties() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTermsOfService() {
        return termsOfService;
    }

    public void setTermsOfService(String termsOfService) {
        this.termsOfService = termsOfService;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
