package com.gitlab.residwi.spring.library.common.resolver;

import com.gitlab.residwi.spring.library.common.model.request.PagingRequest;
import com.gitlab.residwi.spring.library.common.model.request.SortByRequest;
import com.gitlab.residwi.spring.library.common.model.request.SortType;
import com.gitlab.residwi.spring.library.common.properties.PagingProperties;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.core.MethodParameter;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PagingRequestArgumentResolver implements HandlerMethodArgumentResolver {

    public static final String SORT_BY_SEPARATOR = ":";
    public static final String SORT_BY_SPLITTER = ",";
    public static final String EMPTY_STRING = "";

    private final PagingProperties pagingProperties;

    public PagingRequestArgumentResolver(PagingProperties pagingProperties) {
        this.pagingProperties = pagingProperties;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return PagingRequest.class.isAssignableFrom(parameter.getParameterType());
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest webRequest, WebDataBinderFactory webDataBinderFactory) {
        var paging = new PagingRequest();

        paging.setPage(getLong(
                webRequest.getParameter(pagingProperties.getQuery().getPageKey()),
                pagingProperties.getDefaultPage()
        ));

        paging.setItemPerPage(getLong(
                webRequest.getParameter(pagingProperties.getQuery().getItemPerPageKey()),
                pagingProperties.getDefaultItemPerPage()
        ));

        if (paging.getItemPerPage() > pagingProperties.getMaxItemPerPage()) {
            paging.setItemPerPage(pagingProperties.getMaxItemPerPage());
        }

        paging.setSortBy(getSortByList(
                webRequest.getParameter(pagingProperties.getQuery().getSortByKey()),
                pagingProperties
        ));

        return paging;
    }

    private List<SortByRequest> getSortByList(String value, PagingProperties pagingProperties) {
        if (StringUtils.hasText(value)) {
            return toSortByList(value, pagingProperties);
        }

        return Collections.emptyList();
    }

    private Long getLong(String value, Long defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        return toLong(value, defaultValue);
    }

    private Long toLong(String value, Long defaultValue) {
        try {
            return Long.valueOf(value);
        } catch (NumberFormatException ex) {
            return defaultValue;
        }
    }

    private List<SortByRequest> toSortByList(String request, PagingProperties pagingProperties) {
        return Arrays.stream(request.split(SORT_BY_SPLITTER))
                .map(s -> toSortBy(s, pagingProperties))
                .filter(Objects::nonNull)
                .filter(sortBy -> Objects.nonNull(sortBy.getPropertyName()))
                .collect(Collectors.toList());
    }


    private SortByRequest toSortBy(String request, PagingProperties pagingProperties) {
        var sort = request.trim();
        if (!StringUtils.hasText(sort.replace(SORT_BY_SEPARATOR, EMPTY_STRING)) || sort.startsWith(SORT_BY_SEPARATOR)) {
            return null;
        }

        var sortBy = sort.split(SORT_BY_SEPARATOR);

        return new SortByRequest(
                getAt(sortBy, 0, null),
                EnumUtils.getEnum(
                        SortType.class,
                        getAt(sortBy, 1, pagingProperties.getDefaultSortType().name()).toUpperCase()
                ));
    }

    private String getAt(String[] strings, int index, String defaultValue) {
        return strings.length <= index ? defaultValue : strings[index];
    }
}
