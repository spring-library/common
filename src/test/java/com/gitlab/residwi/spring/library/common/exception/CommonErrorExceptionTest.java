package com.gitlab.residwi.spring.library.common.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.common.annotation.Metadata;
import com.gitlab.residwi.spring.library.common.annotation.Metadatas;
import com.gitlab.residwi.spring.library.common.exception.custom.NotFoundException;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.core.MethodParameter;
import org.springframework.core.StandardReflectionParameterNameDiscoverer;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Service;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebInputException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        classes = CommonErrorExceptionTest.Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class CommonErrorExceptionTest {

    private final static ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testThrowable() throws Exception {
        mockMvc.perform(get("/Throwable"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void testConstraintViolationException() throws Exception {
        mockMvc.perform(post("/ConstraintViolationException")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(new Application.HelloRequest())))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testMethodArgumentNotValidException() throws Exception {
        mockMvc.perform(post("/MethodArgumentNotValidException")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(new Application.HelloRequest())))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testHttpMessageNotReadableException() throws Exception {
        mockMvc.perform(get("/HttpMessageNotReadableException"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testServerWebInputException() throws Exception {
        mockMvc.perform(get("/ServerWebInputException"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testResponseStatusException() throws Exception {
        mockMvc.perform(get("/ResponseStatusException"))
                .andExpect(jsonPath("$.errors.reason").value("Error"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void testHttpMediaTypeNotAcceptableException() throws Exception {
        mockMvc.perform(get("/HttpMediaTypeNotAcceptableException"))
                .andExpect(status().isNotAcceptable());
    }

    @Test
    void testHttpMediaTypeNotSupportedException() throws Exception {
        mockMvc.perform(get("/HttpMediaTypeNotSupportedException"))
                .andExpect(status().isUnsupportedMediaType());
    }

    @Test
    void testHttpRequestMethodNotSupportedException() throws Exception {
        mockMvc.perform(post("/HttpRequestMethodNotSupportedException"))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    void testServerErrorException() throws Exception {
        mockMvc.perform(get("/ServerErrorException"))
                .andExpect(status().isInternalServerError());
    }

    @Test
    void testNotFoundException() throws Exception {
        mockMvc.perform(get("/NotFoundException"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testMetadataException() throws Exception {
        mockMvc.perform(get("/MetadataException"))
                .andExpect(jsonPath("$.errors.name").value("NotBlank"))
                .andExpect(jsonPath("$.errors.age").value("NotNull"))
                .andExpect(jsonPath("$.errors['nested.name']").value("NotBlank"))
                .andExpect(jsonPath("$.errors['nested.age']").value("NotNull"))
                .andExpect(jsonPath("$.metadata.errors['name'].message").value("NotBlank"))
                .andExpect(jsonPath("$.metadata.errors['age'].message").value("NotNull"))
                .andExpect(jsonPath("$.metadata.errors['nested.name'].message").value("NotBlank"))
                .andExpect(jsonPath("$.metadata.errors['nested.age'].message").value("NotNull"))
                .andExpect(status().isBadRequest());
    }

    @SpringBootApplication
    public static class Application {

        @RestController
        public static class ExampleController {

            @Autowired
            private ExampleService exampleService;

            @Autowired
            private Validator validator;

            @GetMapping("/Throwable")
            public String throwable() throws Throwable {
                throw new Throwable("Internal Server Error");
            }

            @PostMapping(value = "/ConstraintViolationException", consumes = MediaType.APPLICATION_JSON_VALUE)
            public String constraintViolationException(@RequestBody @Valid HelloRequest request) {
                exampleService.validated(request);
                return "OK";
            }

            @PostMapping(value = "/MethodArgumentNotValidException", consumes = MediaType.APPLICATION_JSON_VALUE)
            public String methodArgumentNotValidException(@RequestBody @Valid HelloRequest request) {
                return "OK";
            }

            @GetMapping("/HttpMessageNotReadableException")
            public String httpMessageNotReadableException() {
                throw new HttpMessageNotReadableException(null, (HttpInputMessage) null);
            }

            @GetMapping("/ServerWebInputException")
            public String serverWebInputException() throws NoSuchMethodException {
                var methodParameter = MethodParameter.forParameter(ExampleService.class.getMethod("validated", HelloRequest.class).getParameters()[0]);
                methodParameter.initParameterNameDiscovery(new StandardReflectionParameterNameDiscoverer());
                throw new ServerWebInputException("Error", methodParameter);
            }

            @GetMapping("/ResponseStatusException")
            public String responseStatusException() {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Error");
            }

            @GetMapping(value = "/HttpMediaTypeNotSupportedException")
            public String httpMediaTypeNotSupportedException() throws HttpMediaTypeNotSupportedException {
                throw new HttpMediaTypeNotSupportedException("Error");
            }

            @GetMapping(value = "/HttpMediaTypeNotAcceptableException")
            public String httpMediaTypeNotAcceptableException() throws HttpMediaTypeNotAcceptableException {
                throw new HttpMediaTypeNotAcceptableException("Error");
            }

            @GetMapping("/HttpRequestMethodNotSupportedException")
            public String httpRequestMethodNotSupportedException() {
                return "OK";
            }

            @GetMapping("/ServerErrorException")
            public String serverErrorException() {
                throw new NullPointerException();
            }

            @GetMapping("/NotFoundException")
            public String notFoundException() {
                throw new NotFoundException();
            }

            @GetMapping("/MetadataException")
            public String metadataValidation() {
                var sampleRequest = new SampleRequest("", null, new NestedSampleRequest());
                var constraintViolations = validator.validate(sampleRequest);
                throw new ConstraintViolationException(constraintViolations);
            }

        }

        public static class SampleRequest {

            @Metadatas(
                    @Metadata(key = "message", value = "NotBlank")
            )
            @NotBlank(message = "NotBlank")
            private String name;

            @Metadatas(
                    @Metadata(key = "message", value = "NotNull")
            )
            @NotNull(message = "NotNull")
            private Integer age;

            @Valid
            private NestedSampleRequest nested;

            public SampleRequest(String name, Integer age, NestedSampleRequest nested) {
                this.name = name;
                this.age = age;
                this.nested = nested;
            }
        }

        public static class NestedSampleRequest {

            @Metadatas(
                    @Metadata(key = "message", value = "NotBlank")
            )
            @NotBlank(message = "NotBlank")
            private String name;

            @Metadatas(
                    @Metadata(key = "message", value = "NotNull")
            )
            @NotNull(message = "NotNull")
            private Integer age;

            public NestedSampleRequest() {
            }
        }

        @Service
        public static class ExampleService {

            public void validated(@Valid HelloRequest request) {
            }
        }

        @RestControllerAdvice
        public static class ErrorController implements CommonErrorException, MessageSourceAware {

            private static final Logger log = LoggerFactory.getLogger(ErrorController.class);

            private MessageSource messageSource;

            @Override
            public Logger getLogger() {
                return log;
            }

            @Override
            public MessageSource getMessageSource() {
                return messageSource;
            }

            @Override
            public void setMessageSource(MessageSource messageSource) {
                this.messageSource = messageSource;
            }
        }

        public static class HelloRequest {

            @NotBlank
            private String name;

            public HelloRequest() {
            }

            public HelloRequest(String name) {
                this.name = name;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

    }
}
