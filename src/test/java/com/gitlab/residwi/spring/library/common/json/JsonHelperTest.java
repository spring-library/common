package com.gitlab.residwi.spring.library.common.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.residwi.spring.library.common.factory.json.JsonAware;
import com.gitlab.residwi.spring.library.common.helper.JsonHelper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = JsonHelperTest.Application.class)
class JsonHelperTest {

    @Autowired(required = false)
    private JsonHelper jsonHelper;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplication webApplication;

    @Test
    void testJsonHelper() {
        assertNotNull(jsonHelper);
    }

    @Test
    void testHello() throws JsonProcessingException {
        HelloResponse helloResponse = new HelloResponse("Resi Dwi");

        assertEquals(jsonHelper.toJson(helloResponse), objectMapper.writeValueAsString(helloResponse));
    }

    @Test
    void testWebApplication() {
        assertNotNull(webApplication.getJsonHelper());
    }

    @Test
    void testWriteTypeReference() {
        List<String> strings = Arrays.asList("Resi", "Dwi");
        String json = webApplication.getJsonHelper().toJson(strings);

        List<String> list = webApplication.getJsonHelper().fromJson(json, new TypeReference<>() {
        });

        assertEquals(strings, list);
    }

    @Test
    void testFromJson() {
        HelloResponse helloResponse = new HelloResponse("Resi Dwi");
        String json = webApplication.getJsonHelper().toJson(helloResponse);

        HelloResponse response = webApplication.getJsonHelper().fromJson(json, HelloResponse.class);
        assertEquals(helloResponse, response);
    }

    @SpringBootApplication
    static class Application {

        @Bean
        public WebApplication webApplication() {
            return new WebApplication();
        }

    }

    static class WebApplication implements JsonAware {

        private JsonHelper jsonHelper;

        public JsonHelper getJsonHelper() {
            return jsonHelper;
        }

        @Override
        public void setJsonHelper(JsonHelper jsonHelper) {
            this.jsonHelper = jsonHelper;
        }
    }

    static class HelloResponse {

        private String name;

        public HelloResponse() {
        }

        public HelloResponse(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            HelloResponse that = (HelloResponse) o;

            return name.equals(that.name);
        }

        @Override
        public int hashCode() {
            return name.hashCode();
        }
    }

}
