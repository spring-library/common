package com.gitlab.residwi.spring.library.common.model;

import com.gitlab.residwi.spring.library.common.helper.ResponseHelper;
import com.gitlab.residwi.spring.library.common.model.request.SortByRequest;
import com.gitlab.residwi.spring.library.common.model.request.SortType;
import com.gitlab.residwi.spring.library.common.model.response.PagingPayload;
import com.gitlab.residwi.spring.library.common.model.response.ResponsePayload;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(
        classes = ResponseTest.Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class ResponseTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testPaging() throws Exception {
        mockMvc.perform(get("/paging"))
                .andExpect(jsonPath("code").value(HttpStatus.OK.value()))
                .andExpect(jsonPath("status").value(HttpStatus.OK.name()))
                .andExpect(jsonPath("data[*]").value(Matchers.contains("Foo", "Bar", "FooBar")))
                .andExpect(jsonPath("paging.page").value(1))
                .andExpect(jsonPath("paging.item_per_page").value(3))
                .andExpect(jsonPath("paging.total_item").value(30))
                .andExpect(jsonPath("paging.total_page").value(10))
                .andExpect(jsonPath("paging.sort_by[0].property_name").value("first_name"))
                .andExpect(jsonPath("paging.sort_by[0].type").value("ASC"));
    }

    @Test
    void testHello() throws Exception {
        mockMvc.perform(get("/hello?name=Foo"))
                .andExpect(jsonPath("code").value(HttpStatus.OK.value()))
                .andExpect(jsonPath("status").value(HttpStatus.OK.name()))
                .andExpect(jsonPath("data.hello").value("Hello Foo"));
    }

    @Test
    void testBadRequest() throws Exception {
        mockMvc.perform(get("/bad-request"))
                .andExpect(jsonPath("code").value(HttpStatus.BAD_REQUEST.value()))
                .andExpect(jsonPath("status").value(HttpStatus.BAD_REQUEST.name()))
                .andExpect(jsonPath("errors.first_name").value(Matchers.contains("NotBlank", "NotNull")));
    }

    @Test
    void testInternalServerError() throws Exception {
        mockMvc.perform(get("/internal-server-error"))
                .andExpect(jsonPath("code").value(HttpStatus.INTERNAL_SERVER_ERROR.value()))
                .andExpect(jsonPath("status").value(HttpStatus.INTERNAL_SERVER_ERROR.name()));
    }

    @Test
    void testOk() throws Exception {
        mockMvc.perform(get("/ok"))
                .andExpect(jsonPath("code").value(HttpStatus.OK.value()))
                .andExpect(jsonPath("status").value(HttpStatus.OK.name()));
    }

    @Test
    void testUnauthorized() throws Exception {
        mockMvc.perform(get("/unauthorized"))
                .andExpect(jsonPath("code").value(HttpStatus.UNAUTHORIZED.value()))
                .andExpect(jsonPath("status").value(HttpStatus.UNAUTHORIZED.name()));
    }

    @Test
    void testRedirect() throws Exception {
        mockMvc.perform(get("/redirect"))
                .andExpect(jsonPath("code").value(HttpStatus.PERMANENT_REDIRECT.value()))
                .andExpect(jsonPath("status").value(HttpStatus.PERMANENT_REDIRECT.name()));
    }

    @Test
    void testCreated() throws Exception {
        mockMvc.perform(get("/created"))
                .andExpect(jsonPath("code").value(HttpStatus.CREATED.value()))
                .andExpect(jsonPath("status").value(HttpStatus.CREATED.name()));
    }

    @Test
    void testConflict() throws Exception {
        mockMvc.perform(get("/conflict"))
                .andExpect(jsonPath("code").value(HttpStatus.CONFLICT.value()))
                .andExpect(jsonPath("status").value(HttpStatus.CONFLICT.name()));
    }

    @Test
    void testForbidden() throws Exception {
        mockMvc.perform(get("/forbidden"))
                .andExpect(jsonPath("code").value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath("status").value(HttpStatus.FORBIDDEN.name()));
    }

    @SpringBootApplication
    public static class Application {

        @RestController
        public static class ExampleController {

            @GetMapping(value = "/paging", produces = MediaType.APPLICATION_JSON_VALUE)
            public ResponsePayload<List<String>> paging() {
                var sortByRequests = List.of(new SortByRequest("first_name", SortType.ASC));

                return ResponseHelper.ok(Arrays.asList("Foo", "Bar", "FooBar"),
                        new PagingPayload(1L, 10L, 3L, 30L, sortByRequests));
            }

            @GetMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_VALUE)
            public ResponsePayload<SayHelloResponse> response(@RequestParam("name") String name) {
                return ResponseHelper.ok(new SayHelloResponse(String.format("Hello %s", name)));
            }

            @GetMapping("/bad-request")
            public ResponsePayload<String> badRequest() {
                return ResponseHelper.badRequest(Map.of("first_name", Arrays.asList("NotBlank", "NotNull")));
            }

            @GetMapping("/internal-server-error")
            public ResponsePayload<String> internalServerError() {
                return ResponseHelper.internalServerError();
            }

            @GetMapping("/ok")
            public ResponsePayload<String> ok() {
                return ResponseHelper.ok();
            }

            @GetMapping("/unauthorized")
            public ResponsePayload<String> unauthorized() {
                return ResponseHelper.unauthorized();
            }

            @GetMapping("/redirect")
            public ResponsePayload<String> redirect() {
                return ResponseHelper.status(HttpStatus.PERMANENT_REDIRECT);
            }

            @GetMapping("/created")
            public ResponsePayload<String> created() {
                return ResponseHelper.status(HttpStatus.CREATED);
            }

            @GetMapping("/conflict")
            public ResponsePayload<String> conflict() {
                return ResponseHelper.status(HttpStatus.CONFLICT);
            }

            @GetMapping("/forbidden")
            public ResponsePayload<String> forbidden() {
                return ResponseHelper.status(HttpStatus.FORBIDDEN);
            }

        }

        public static class SayHelloResponse {

            private String hello;

            public SayHelloResponse(String hello) {
                this.hello = hello;
            }

            public String getHello() {
                return hello;
            }

            public void setHello(String hello) {
                this.hello = hello;
            }
        }

    }
}
