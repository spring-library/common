package com.gitlab.residwi.spring.library.common.resolver;

import com.gitlab.residwi.spring.library.common.helper.PagingHelper;
import com.gitlab.residwi.spring.library.common.model.request.PagingRequest;
import com.gitlab.residwi.spring.library.common.model.response.PagingPayload;
import com.gitlab.residwi.spring.library.common.properties.PagingProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(
        classes = PagingRequestTest.Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class PagingRequestTest {

    private final static PagingProperties pagingProperties = new PagingProperties();

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testPagingRequest() throws Exception {
        mockMvc.perform(get("/paging?page=1&item_per_page=100&sort_by=first_name:ASC,last_name:DESC"))
                .andExpect(jsonPath("$.page").value(1))
                .andExpect(jsonPath("$.item_per_page").value(100))
                .andExpect(jsonPath("$.sort_by[0].property_name").value("first_name"))
                .andExpect(jsonPath("$.sort_by[0].type").value("ASC"))
                .andExpect(jsonPath("$.sort_by[1].property_name").value("last_name"))
                .andExpect(jsonPath("$.sort_by[1].type").value("DESC"));
    }

    @Test
    void testDefaultPagingRequest() throws Exception {
        mockMvc.perform(get("/paging-request"))
                .andExpect(jsonPath("$.page").value(pagingProperties.getDefaultPage()))
                .andExpect(jsonPath("$.item_per_page").value(pagingProperties.getDefaultItemPerPage()))
                .andExpect(jsonPath("$.sort_by").isEmpty());
    }

    @Test
    void testPaging() throws Exception {
        mockMvc.perform(get("/paging?page=1&item_per_page=100&sort_by=first_name:ASC,last_name:DESC"))
                .andExpect(jsonPath("$.page").value(1))
                .andExpect(jsonPath("$.item_per_page").value(100))
                .andExpect(jsonPath("$.total_page").value(100))
                .andExpect(jsonPath("$.total_item").value(100 * 100))
                .andExpect(jsonPath("$.sort_by[0].property_name").value("first_name"))
                .andExpect(jsonPath("$.sort_by[0].type").value("ASC"))
                .andExpect(jsonPath("$.sort_by[1].property_name").value("last_name"))
                .andExpect(jsonPath("$.sort_by[1].type").value("DESC"));
    }

    @Test
    void testDefaultPaging() throws Exception {
        mockMvc.perform(get("/paging"))
                .andExpect(jsonPath("$.page").value(pagingProperties.getDefaultPage()))
                .andExpect(jsonPath("$.item_per_page").value(pagingProperties.getDefaultItemPerPage()))
                .andExpect(jsonPath("$.total_page").value(100))
                .andExpect(jsonPath("$.total_item").value(100 * pagingProperties.getDefaultItemPerPage()))
                .andExpect(jsonPath("$.sort_by").isEmpty());
    }

    @SpringBootApplication
    public static class Application {

        @RestController
        public static class ExampleController {

            @GetMapping(value = "/paging-request", produces = MediaType.APPLICATION_JSON_VALUE)
            public PagingRequest pagingRequest(PagingRequest pagingRequest) {
                return pagingRequest;
            }

            @GetMapping(value = "/paging", produces = MediaType.APPLICATION_JSON_VALUE)
            public PagingPayload paging(PagingRequest pagingRequest) {
                return PagingHelper.toPaging(pagingRequest, 100L, 100 * pagingRequest.getItemPerPage());
            }

        }

    }
}
