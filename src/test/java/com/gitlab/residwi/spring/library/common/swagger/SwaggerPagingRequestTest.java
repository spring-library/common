package com.gitlab.residwi.spring.library.common.swagger;

import com.gitlab.residwi.spring.library.common.annotation.PagingRequestInQuery;
import com.gitlab.residwi.spring.library.common.helper.PagingHelper;
import com.gitlab.residwi.spring.library.common.model.request.PagingRequest;
import com.gitlab.residwi.spring.library.common.model.response.PagingPayload;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(
        classes = SwaggerPagingRequestTest.Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class SwaggerPagingRequestTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testPaging() throws Exception {
        mockMvc.perform(get("/v3/api-docs"))
                .andExpect(jsonPath("$.paths.['/paging'].get.parameters[*].name").value(Matchers.contains("page", "item_per_page", "sort_by")))
                .andExpect(jsonPath("$.paths.['/paging-request'].get.parameters[*].name").value(Matchers.contains("page", "item_per_page", "sort_by")))
                .andExpect(jsonPath("$.components.parameters.queryPagingRequestPage").isNotEmpty())
                .andExpect(jsonPath("$.components.parameters.queryPagingRequestItemPerPage").isNotEmpty())
                .andExpect(jsonPath("$.components.parameters.queryPagingRequestSortBy").isNotEmpty());
    }

    @SpringBootApplication
    public static class Application {

        @RestController
        public static class PagingController {

            @PagingRequestInQuery
            @GetMapping(value = "/paging-request", produces = MediaType.APPLICATION_JSON_VALUE)
            public PagingRequest pagingRequest(PagingRequest pagingRequest) {
                return pagingRequest;
            }

            @PagingRequestInQuery
            @GetMapping(value = "/paging", produces = MediaType.APPLICATION_JSON_VALUE)
            public PagingPayload paging(PagingRequest pagingRequest) {
                return PagingHelper.toPaging(pagingRequest, 100L, 100 * pagingRequest.getItemPerPage());
            }

        }
    }
}
