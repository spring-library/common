package com.gitlab.residwi.spring.library.common.swagger;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(
        classes = SwaggerTest.Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
class SwaggerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testSwaggerUi() throws Exception {
        mockMvc.perform(get("/swagger-ui.html"))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    void testApiDocs() throws Exception {
        mockMvc.perform(get("/v3/api-docs"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.info.title").value("Resi Dwi"))
                .andExpect(jsonPath("$.info.version").value("1.0.0"));
    }

    @Test
    void testController() throws Exception {
        mockMvc.perform(get("/v3/api-docs"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.paths.['/']").isNotEmpty())
                .andExpect(jsonPath("$.paths.['/hello']").isNotEmpty())
                .andExpect(jsonPath("$.paths.['/example']").isNotEmpty());
    }

    @SpringBootApplication
    public static class Application {

        @RestController
        public static class HomeController {

            @GetMapping("/")
            public String home() {
                return "Home";
            }

            @GetMapping(value = "/example", produces = MediaType.APPLICATION_JSON_VALUE)
            public ExampleResponse example() {
                return new ExampleResponse("Hello");
            }

            @PostMapping(value = "/hello", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
            public ExampleResponse hello(@RequestBody ExampleRequest request) {
                return new ExampleResponse("Hello " + request.getName());
            }

            public static class ExampleResponse {

                private String hello;

                public ExampleResponse(String hello) {
                    this.hello = hello;
                }
            }

            public static class ExampleRequest {

                private String name;

                public ExampleRequest(String name) {
                    this.name = name;
                }

                public String getName() {
                    return name;
                }
            }

        }

    }
}
